<?php

/**
 * @file
 * Donate entity class.
 */

/**
 * The class used for DonateEntity entities.
 */
class DonateEntity extends Entity {
  /**
   * Construct handler.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'donate_entity');
  }

  /**
   * Default Label handler.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * Default URI handler.
   */
  protected function defaultUri() {
    return array('path' => 'donate_entities/donate_entity/' . $this->deid);
  }


}

/**
 * The Controller for DonateEntity entities.
 */
class DonateEntityController extends EntityAPIController {
  /**
   * Construct handler.
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Create a donate_entity.
   *
   * We first set up the values that are specific to our donate_entity schema but then also go through the EntityAPIController function.
   *
   * @param array $values
   *   An array of values that are specific to our donate entity.
   *
   * @return new DonateEntity
   *   A donate_entity object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our donate_entity
    $values += array(
      'deid' => '',
      'amount' => 0,
      'mail' => '',
      'uid' => 0,
      'pid' => 0,
      'created' => time(),
    );

    $donate_entity = parent::create($values);
    return $donate_entity;
  }

}
