<?php

/**
 * @file
 * Donate entity type class.
 */

/**
 * The class used for donate_entity type entities.
 */
class DonateEntityType extends Entity {

  public $type;
  public $label;

  /**
   * Construct handler.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'donate_entity_type');
  }

}

/**
 * The Controller for donate_entity entities.
 */
class DonateEntityTypeController extends EntityAPIControllerExportable {
  /**
   * Construct handler.
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Create a donate_entity type.
   *
   * We first set up the values that are specific to our donate_entity type schema but then also go through the EntityAPIController function.
   *
   * @param array $values
   *   An array of values.
   *
   * @return new DonateEntityType
   *   A donate_entity type object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our donate_entity
    $values += array(
      'debid' => '',
      'is_new' => TRUE,
      'type' => '',
      'label' => '',
      'description' => '',
      'suggestamt' => '',
      'otheramt' => '',
      'intro' => '',
      'intro_format' => '',
      'pmid' => 0,
      'currency_code' => variable_get('donate_entity_currencycode', 'AUD'),
      'weight' => -10,
      'data' => '',
    );
    $donate_entity_type = parent::create($values);
    return $donate_entity_type;
  }

}
