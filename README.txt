
Eventbrite API for Drupal 7.x
----------------------------
This module provides a fieldable entity that allows the user to accept donations
on their website.

Installation
------------
This is a standard and simple module. Just put the module into your
sites/all/modules folder, then go to the modules page and enable it. You will
also need the payment module and another module that integrates with the payment
module to accept payments.

Using Donate Entity
-------------------
From the config page, admin/config/content/donate_entity, you can set your
options.


Maintainers
-----------

- kafmil
