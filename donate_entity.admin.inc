<?php

/**
 * @file
 * The form fields in donate_entities_edit_form need to be checked for correct validation in particular, validation for saving and displaying the user autocomplete fields the addresses need to be added to the memberships as separate fields in order to properly use the Customer Reference Profiles. svbfields needs to be corrected to also validate the email address. Fields on the admin form should not be editable, eg name, id, order id etc. We also need to add in permissions around what users can CRUD on the member form.
 */

/**
 * @file
 * Donate entity editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class DonateEntityUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    // Add menu items to add each different type of entity.
    foreach (donate_entity_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'donate_entity_form_wrapper',
        'page arguments' => array(donate_entity_create(array('type' => $type->type))),
        'access arguments' => array('create_any_donate_entity'),
        'file' => 'donate_entity.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module']),
      );
    }

    // Loading and editing donate_entity entities.
    $items[$this->path . '/donate_entity/' . $wildcard] = array(
      'page callback' => 'donate_entity_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'donate_entity.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );
    $items[$this->path . '/donate_entity/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/donate_entity/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'donate_entity_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('delete', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'donate_entity.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    // Menu item for viewing donate_entitys
    $items[$this->path . '/donate_entity/' . $wildcard] = array(
      // 'title' => 'Title',
      'title callback' => 'donate_entity_page_title',
      'title arguments' => array(4),
      'page callback' => 'donate_entity_page_view',
      'page arguments' => array(4),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('view', 4),
    );

    $items[$this->path . '/donate_entity/' . $wildcard . '/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    $items[$this->path . '/donate_entity/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'donate_entity_form_wrapper',
      'page arguments' => array(4),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('update', 4),
      'weight' => 0,
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'donate_entity.admin.inc',
      'file path' => drupal_get_path('module', 'donate_entity'),
    );

    // Provide these views for the user dashboard
    // Menu item for viewing donate_entitys
    $items['user/%user/donate_entity/' . $wildcard] = array(
      // 'title' => 'Title',
      'title callback' => 'donate_entity_page_title',
      'title arguments' => array(3),
      'page callback' => 'donate_entity_page_view',
      'page arguments' => array(3),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('view', 1),
    );

    $items['user/%user/donate_entity/' . $wildcard . '/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    $items['user/%user/donate_entity/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'donate_entity_form_wrapper',
      'page arguments' => array(3),
      'access callback' => 'donate_entity_access',
      'access arguments' => array('update', 1),
      'weight' => 0,
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'donate_entity.admin.inc',
      'file path' => drupal_get_path('module', 'donate_entity'),
    );

    return $items;
  }

  /**
   * Create the markup for the add donate_entity Entities page within the class so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('donate_entity_add_list', array('content' => $content));
  }

}

/**
 * Form callback wrapper: create or edit a donate_entity.
 *
 * @param object $donate_entity
 *   The donate_entity object being edited by this form.
 *
 * @see donate_entity_edit_form()
 */
function donate_entity_form_wrapper($donate_entity) {
  // Add the breadcrumb for the form's location.
  donate_entity_set_breadcrumb();
  return drupal_get_form('donate_entity_edit_form', $donate_entity);
}

/**
 * Form callback wrapper: delete a donate_entity.
 *
 * @param object $donate_entity
 *   The donate_entity object being edited by this form.
 *
 * @see donate_entity_edit_form()
 */
function donate_entity_delete_form_wrapper($donate_entity) {
  // Add the breadcrumb for the form's location.
  donate_entity_set_breadcrumb();
  return drupal_get_form('donate_entity_delete_form', $donate_entity);
}

/**
 * Form callback: create or edit a donate_entity.
 *
 * @param object $donate_entity
 *   The donate_entity object to edit or for a create form an empty donate_entity object with only a donate_entity type defined.
 */
function donate_entity_edit_form($form, &$form_state, $donate_entity) {
  global $user;

  drupal_add_js(drupal_get_path('module', 'donate_entity') . '/js/donate_entity.js', 'file');

  $donatetype = donate_entity_type_load($donate_entity->type);

  $donatesugamt = $donatetype->suggestamt;
  $donateothamt = $donatetype->otheramt;
  $donatepmid = $donatetype->pmid;
  $donatecurrencycode = $donatetype->currency_code;
  // $donatedescription = $donatetype->description;
  $donate_weight = $donatetype->data['weight'];

  // Check if the thank you message should be displayed.
  // This is only true when the form has been submitted and the option to show the message is 'Normal'.
  // @see donate_entity_edit_form_submit().
  if (isset($_SESSION['donate_thankyou'])) {
    // Add the thank you message to the form.
    $form['#prefix'] = $_SESSION['donate_thankyou'];
    // Make sure the message doesn't display twice.
    unset($_SESSION['donate_thankyou']);
  }

  if (!empty($donatetype->redirect)) {
    $form_state['redirect_to'] = $donatetype->redirect;
  }

  // Display the intro text.
  if (!empty($donatetype->intro)) {
    $form['intro'] = array(
      '#markup' => $donatetype->intro,
      '#weight' => $donate_weight['intro'],
    );
  }
  
  // Add the donate entity type so it can be used in the submit function.
  $form_state['donate_entity_type'] = $donatetype;

  // If the user is logged in, or if there is already a user for the current donation, select them.
  // This field is only visible by admins.
  $donatename = user_load($donate_entity->uid);
  $form['uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Donor'),
    '#default_value' => isset($donatename->name) ? $donatename->name : isset($user->name) ? $user->name : '',
    '#maxlength' => 255,
    '#required' => FALSE,
    '#weight' => $donate_weight['uid'],
    '#autocomplete_path' => 'user/autocomplete',
    '#access' => user_access('edit_any_donate_entity'),
  );

  $form['pid'] = array(
    '#type' => 'hidden',
    '#title' => t('Payment ID'),
    '#default_value' => isset($donate_entity->pid) ? $donate_entity->pid : 0,
    '#required' => FALSE,
  );

  $form['description'] = array(
    '#type' => 'hidden',
    '#title' => t('Description'),
    '#default_value' => isset($donatetype->description) ? $donatetype->description : 0,
    '#maxlength' => 255,
  );

  $form['mail'] = array(
    '#type' => 'hidden',
    '#title' => t('Email'),
    '#default_value' => isset($donate_entity->mail) ? $donate_entity->mail : isset($user->mail) ? $user->mail : '',
    '#weight' => $donate_weight['mail'],
    '#maxlength' => 255,
  );
  if ($donatetype->data['email']['email_display']) {
    $form['mail']['#type'] = 'textfield';
    if ($donatetype->data['email']['email_required']) {
      $form['mail']['#required'] = TRUE;
    }
  }

  if (!empty($donatesugamt)) {
    $currencies = _donate_entity_get_cur_symbols();
    $donatesugamt = str_ireplace($currencies, '', $donatesugamt);
    $donatesugamt = explode("\n", $donatesugamt);
    $default = array_search($donate_entity->amount, $donatesugamt, TRUE);
    $form['sugamount'] = array(
      '#type' => 'radios',
      '#title' => t('Donation Amount'),
      '#options' => $donatesugamt,
      '#default_value' => isset($default) ? $default : NULL,
      '#maxlength' => 255,
      '#weight' => $donate_weight['amount'],
      '#required' => FALSE,
    );
  }

  if (!empty($donateothamt)) {
    $form['amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Donation Amount'),
      '#default_value' => isset($donate_entity->amount) ? $donate_entity->amount : 0,
      '#maxlength' => 255,
      '#weight' => $donate_weight['amount'] + 0.5,
      '#required' => TRUE,
    );
  }

  $form['data']['#tree'] = TRUE;

  // Add the field related form elements.
  $form_state['donate_entity'] = $donate_entity;
  field_attach_form('donate_entity', $donate_entity, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  // Add the payment.
  if (!$donatecurrencycode || !$donatepmid) {
    $form['#access'] = FALSE;
    drupal_set_message(t('The payment method or currency codes have not been setup for this donation form.'), 'error');
    return $form;
  }

  /*$form['actions']['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Save Donation'),
  '#submit' => $submit + array('donate_entity_edit_form_submit'),
  );*/

  if (!empty($donate_entity->amount)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Donation'),
      '#submit' => $submit + array('donate_entity_form_submit_delete'),
      '#access' => user_access('edit_any_donate_entity') || user_access('edit_own_donate_entity'),
    );
  }
  else {
    $payment_method = entity_load_single('payment_method', $donatepmid);

    $currencies = $payment_method->controller->currencies;
    if (isset($currencies[$donatecurrencycode]['minimum'])) {
      $form['amount']['#minimum_amount'] = $currencies[$donatecurrencycode]['minimum'];
    }
    if (isset($currencies[$donatecurrencycode]['maximum'])) {
      $form['amount']['#maximum_amount'] = $currencies[$donatecurrencycode]['maximum'];
    }

    $payment = new Payment(array(
      'context' => 'donate_entity',
      'context_data' => array(
        'destination' => $_GET['q'],
      ),
      'currency_code' => $donatecurrencycode,
      'description' => $donatetype->description,
      'finish_callback' => 'donate_entity_finish',
      'method' => $payment_method,
    ));
    $form_info = payment_form_embedded($form_state, $payment, array($donatepmid));
    unset($form_info['elements']['payment_line_items']);
    unset($form_info['elements']['payment_status']);
    $form = array_merge($form, $form_info['elements']);

    $form['pay'] = array(
      '#value' => t('Pay'),
      '#type' => 'submit',
      '#prefix' => '<img id="edit-pay-img" src="/' . drupal_get_path('module', 'donate_entity') . '/images/ajaxloader.gif" style="display:none; height: 28px; padding-left: 16px;">',
      '#weight' => $donate_weight['pay'],
      '#submit' => array_merge(array('donate_entity_edit_form_submit'), $form_info['submit']),
    );
  }

  // Add the default field elements.
  $form['#donate_entity'] = $donate_entity;

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'donate_entity_edit_form_validate';

  return $form;
}

/**
 * Form API validate callback for the donate_entity form.
 */
function donate_entity_edit_form_validate(&$form, &$form_state) {
  $payment =& $form_state['payment'];
  $donate_entity = $form_state['donate_entity'];

  $currencies = _donate_entity_get_cur_symbols();
  $form_state['values']['amount'] = trim(str_ireplace($currencies, '', $form_state['values']['amount']));
  if (!is_numeric($form_state['values']['amount']) || $form_state['values']['amount'] <= 0) {
    form_set_error('amount', t('The amount field numst be a positive number.'));
  }

  $user = user_load_by_name($form_state['values']['uid']);
  if (!isset($user)) {
    form_set_error('uid', t('You must select a valid user for the Member field.'));
  }

  // Notify field widgets to validate their data.
  field_attach_form_validate('donate_entity', $donate_entity, $form, $form_state);

  $payment->setLineItem(new PaymentLineItem(array(
    'amount' => $form_state['values']['amount'],
    'description' => $form_state['values']['description'],
    'name' => 'donate_entity',
  )));
  try {
    // $success = $payment->method->validate($payment);
  }
  catch (PaymentValidationException $e) {
    $message = strlen($e->getMessage()) ? $e->getMessage() : t('The payment cannot be executed.');
    form_error($form['amount'], $message);
  }
}

/**
 * Form API submit callback for the donate_entity form.
 *
 * @todo remove hard-coded link
 */
function donate_entity_edit_form_submit(&$form, &$form_state) {
  $payment = $form_state['payment'];

  try {
    $payment->execute();
  }
  catch (PaymentValidationException $e) {
    $message = strlen($e->getMessage()) ? $e->getMessage() : t('The payment cannot be executed.');
    form_error($form['amount'], $message);
  }
  entity_save('payment', $payment);

  if ($payment->pid) {
    $donate_entity = entity_ui_controller('donate_entity')->entityFormSubmitBuildEntity($form, $form_state);
    $donate_type = $form_state['donate_entity_type'];
    // Save the donate_entity and go back to the list of donate_entities
    if ($donate_type->data['email']['email_match'] && $form_state['values']['uid'] == '') {
      $donuser = user_load_by_mail($form_state['values']['mail']);
      if (isset($donuser) && isset($donuser->uid)) {
        $form_state['values']['uid'] = $donuser->uid;
      }
    }
    $donateuser = user_load_by_name($form_state['values']['uid']);
    $donate_entity->uid = $donateuser->uid;
    $donate_entity->pid = $payment->pid;

    // Add in created and changed times.
    if ($donate_entity->is_new = isset($donate_entity->is_new) ? $donate_entity->is_new : 0) {
      $donate_entity->created = time();
    }

    $donate_entity->save();
    if (!empty($form_state['redirect_to'])) {
      unset($_GET['destination']);
      $form_state['redirect'] = $form_state['redirect_to'];
    }
  
    // Check to see if a Thank You message should be displayed.
    if (isset($donate_type->thankyou) && $donate_type->thankyou != '') {
      // Work out if this should be displayed as a dsm or on the form page.
      if (isset($donate_type->thankyou_type)) {
        if ($donate_type->thankyou_type == 'dsm') {
          drupal_set_message($donate_type->thankyou); 
        }
        if ($donate_type->thankyou_type == 'normal') {
          // Add the thank you message to the session, so it can be displayed on the form page.
          $_SESSION['donate_thankyou'] = $donate_type->thankyou;
        }
      }
    }

    // Prepare for the email to be sent.
    $values = $form_state['values'];
    $language = language_default();
    $securepay_ref_no = (isset($payment->sp->TransactionId) && $payment->sp->TransactionId != '') ? $payment->sp->TransactionId : 0;
    // Check if there is a reference number (i.e. the payment succeeded).
    if ($securepay_ref_no != 0) {
      $donation_date = date('d M Y');
      $donor_title = _donate_entity_return_submitted_value('field_title', $values);
      $first_name = _donate_entity_return_submitted_value('field_first_name', $values);
      $last_name = _donate_entity_return_submitted_value('field_last_name', $values);
      $full_name = $first_name . ' ' . $last_name;
      $address_array = array();
      $address_parts = array(
        'field_address_street',
        'field_address_suburb',
        'field_address_state',
        'field_address_postcode',
        'field_address_country',
      );
      foreach ($address_parts as $address_part) {
        if (isset($values[$address_part]) && is_array($values[$address_part]) && !empty($values[$address_part])) {
          $address_array[] = $values[$address_part][LANGUAGE_NONE][0]['value'];
        }
      }
      $address_full = implode(", ", $address_array);
      $donor_optin = _donate_entity_return_submitted_value('field_optin', $values);
      $donor_optin = ($donor_optin) ? 'Yes' : 'No';
      $donation_amount = _donate_entity_return_submitted_value('amount', $values);
      $donation_entity = $form_state['donate_entity'];
      $donation_to = _donate_entity_return_submitted_value('field_donate_to', $values);
      if ($donation_to == '') {
        $donation_to = $donation_entity->type;
      }
      $donor_email = check_plain(_donate_entity_return_submitted_value('mail', $values));
      $params = array(
        '@securepay_ref_no' => $securepay_ref_no,
        '@donation_date' => $donation_date,
        '@donor_title' => $donor_title,
        '@donor_full_name' => $full_name,
        '@donor_address' => $address_full,
        '!donor_email' => $donor_email,
        '@donor_optin' => $donor_optin,
        '@donation_amount' => $donation_amount,
        '!donation_to' => $donation_to,
      );
      $from = 'no-reply@alzheimers.org.au';
      // Change the below to true when you are ready to send the email.
      $send = TRUE;
      // Send the email to the donor.
      drupal_mail('donate_entity', 'donor', $donor_email, $language, $params, $from, $send);
      // TODO: Check if the email was sent successfully.
      // Send the email to AZA.
      $aza_email = 'website@alzheimers.org.au';
      drupal_mail('donate_entity', 'aza', $aza_email, $language, $params, $from, $send);
      // TODO: Check if the email was sent successfully.
    }
  }

}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function donate_entity_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/donate_entities/donate_entity/' . $form_state['donate_entity']->deid . '/delete';
}

/**
 * Form callback: confirmation form for deleting a donate_entity.
 *
 * @param object $donate_entity
 *   The donate_entity to delete.
 *
 * @see confirm_form()
 */
function donate_entity_delete_form($form, &$form_state, $donate_entity) {
  $form_state['donate_entity'] = $donate_entity;

  $form['#submit'][] = 'donate_entity_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete donate_entity %name?', array('%name' => $donate_entity->name)),
    'donate_entity',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for donate_entity_delete_form.
 */
function donate_entity_delete_form_submit($form, &$form_state) {
  $donate_entity = $form_state['donate_entity'];

  donate_entity_delete($donate_entity);

  drupal_set_message(t('The donate_entity %name has been deleted.', array('%name' => $donate_entity->name)));
  watchdog('donate_entity', 'Deleted donate_entity %name.', array('%name' => $donate_entity->name));

  $form_state['redirect'] = 'admin/content/donate_entities';
}

/**
 * Page to add donate_entity Entities.
 *
 * @todo Pass this through a proper theme function
 */
function donate_entity_add_page() {
  $controller = entity_ui_controller('DonateEntity');
  return $controller->addPage();
}

/**
 * Displays the list of available donate_entity types for donate_entity creation.
 *
 * @ingroup themeable
 */
function theme_donate_entity_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="donate_entity-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer_donate_entity_types')) {
      $output = '<p>' . t('donate_entity Entities cannot be added because you have not created any donate_entity types yet. Go to the <a href="@create-donate_entity-type">donate_entity type creation page</a> to add a new donate_entity type.', array('@create-donate_entity-type' => url('admin/structure/donate_entity_type/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No donate_entity types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Sets the breadcrumb for administrative donate_entity pages.
 */
function donate_entity_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Donate Entities'), 'admin/content/donate_entities'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Sets the breadcrumb for administrative donate_entity pages.
 */
function donate_entity_type_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/structure'),
    l(t('donate_entities'), 'admin/structure/donate_entity_type'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Helper function: Return relevant value otherwise blank string.
 */
function _donate_entity_return_submitted_value($element_id, $array) {
  $value = '';
  if (isset($array[$element_id]) && $array[$element_id] != '') {
    if (is_array($array[$element_id]) && !empty($array[$element_id])) {
      $value = $array[$element_id][LANGUAGE_NONE][0]['value'];
    }
    else {
      $value = $array[$element_id];
    }
  }

  return $value;
}
