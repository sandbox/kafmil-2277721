/**
 * Javascript for donate entity module.
 */

(function ($, Drupal, window, document, undefined) {
    $(document).ready(function(){
        $('#edit-pay').click(function() {
            $(this).hide();
            $(this).siblings('#edit-pay-img').show();
        });
        $('input[id^="edit-sugamount"]').change(function() {
            $('#edit-amount').val($("label[for='" + $(this).attr("id") + "']").text().trim());
        });
        $('#edit-payment-method-pmid-title').hide();
    });
})(jQuery, Drupal, this, this.document);
