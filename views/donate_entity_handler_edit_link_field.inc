<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying edit links as fields.
 */

class DonateEntityHandlerEditLinkField extends DonateEntityHandlerLinkField {
  /**
   * Construct handler.
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }

  /**
   * Render handler.
   */
  public function render($values) {
    $type = $values->{$this->aliases['type']};

    // Creating a dummy donate_entity to check access against.
    $dummy_donate_entity = (object) array('type' => $type);
    if (!donate_entity_access('edit', $dummy_donate_entity)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $deid = $values->{$this->aliases['deid']};

    return l($text, 'admin/content/donate_entities/donate_entity/' . $deid . '/edit');
  }
}
