<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying links to entities as fields.
 */

class DonateEntityHandlerLinkField extends views_handler_field {
  /**
   * Construct handler.
   */
  public function construct() {
    parent::construct();

    $this->additional_fields['deid'] = 'deid';
    $this->additional_fields['type'] = 'type';
  }

  /**
   * Option definition handler.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  /**
   * Options form handler.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  /**
   * Query handler.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render handler.
   */
  public function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $deid = $values->{$this->aliases['deid']};

    return l($text, 'admin/content/donate_entities/donate_entity/' . $deid);
  }
}
