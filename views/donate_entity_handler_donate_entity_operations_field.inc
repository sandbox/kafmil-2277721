<?php

/**
 * @file
 * Donate entity handler (operations field).
 */

/**
 * This field handler aggregates operations that can be done on a donate_entity under a single field providing a more flexible way to present them in a view.
 */
class DonateEntityHandlerDonateEntityOperationsField extends views_handler_field {
  /**
   * Construct handler.
   */
  public function construct() {
    parent::construct();

    $this->additional_fields['deid'] = 'deid';
  }

  /**
   * Query handler.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render handler.
   */
  public function render($values) {

    $links = menu_contextual_links('donate_entity', 'admin/content/donate_entities/donate_entity', array($this->get_value($values, 'deid')));

    if (!empty($links)) {
      return theme(
        'links', array(
          'links' => $links,
          'attributes' => array(
            'class' => array(
              'links', 'inline', 'operations',
            ),
          ),
        )
      );
    }
  }
}
