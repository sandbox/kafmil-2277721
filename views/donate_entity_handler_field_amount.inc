<?php

/**
 * @file
 * Contains the basic amount field handler.
 */

/**
 * Field handler to allow rendering of the amount using currency formatting.
 */
class DonateEntityHandlerFieldAmount extends views_handler_field {
  /**
   * Init handler.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);

    $this->additional_fields['currency_code'] = 'currency_code';
  }

  /**
   * Option definition handler.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['display_format'] = array('default' => 'formatted');

    return $options;
  }

  /**
   * Provide the currency format option.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['display_format'] = array(
      '#type' => 'select',
      '#title' => t('Display format'),
      '#options' => array(
        'formatted' => t('Currency formatted amount'),
        'raw' => t('Raw amount'),
      ),
      '#default_value' => $this->options['display_format'],
    );
  }

  /**
   * Render handler.
   */
  public function render($values) {
    $value = $this->get_value($values);
    // $currency_code = $this->get_value($values, 'currency_code');
    switch ($this->options['display_format']) {
      case 'formatted':
        return '$' . number_format($value / 10, 2);

      case 'raw':
        // Format the price as a number.
        return number_format($value, 2);
    }
  }
}
