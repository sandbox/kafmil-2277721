<?php

/**
 * @file
 * Providing extra functionality for the donate_entity UI via views.
 */

/**
 * Implements hook_views_data().
 */
function donate_entity_views_data_alter(&$data) {
  $data['donate_entity']['link_donate_entity'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the donate_entity.'),
      'handler' => 'DonateEntityHandlerLinkField',
    ),
  );
  $data['donate_entity']['edit_donate_entity'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the donate_entity.'),
      'handler' => 'DonateEntityHandlerEditLinkField',
    ),
  );
  $data['donate_entity']['delete_donate_entity'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the donate_entity.'),
      'handler' => 'DonateEntityHandlerDeleteLinkField',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows donate_entities/donate_entity/%deid/op
  $data['donate_entity']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this donate_entity.'),
      'handler' => 'DonateEntityHandlerDonateEntityOperationsField',
    ),
  );
  // Add relationship for the uid and guid fields on memberships for views.
  $data['donate_entity']['uid'] = array(
    'title' => 'Donation Owner',
    'help' => 'The user who made the donation',
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
      'title' => 'Name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => 'Donation Owner',
      'help' => 'The user who made the donation',
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('User', array(), array('context' => 'A druplal user')),
    ),
  );
  // Add in the formatters for created date.
  $data['donate_entity']['created'] = array(
    'title' => 'Created Date',
    'help' => 'The date the donation was made',
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
      'is date' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
      'is date' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'is date' => TRUE,
    ),
  );

  $data['donate_entity']['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['donate_entity']['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['donate_entity']['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['donate_entity']['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['donate_entity']['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['donate_entity']['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  // Add in the formatters for created date.
  $data['donate_entity']['amount'] = array(
    'title' => 'Amount',
    'help' => 'The amount of the donation.',
    'field' => array(
      'handler' => 'DonateEntityHandlerFieldAmount',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Add in the formatters for created date.
  $data['donate_entity']['pid'] = array(
    'title' => t('Payment ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'title' => t('Payment'),
      'base' => 'payment',
      'field' => 'pid',
      'handler' => 'views_handler_relationship',
      'label' => t('Payment'),
    ),
  );
}

/**
 * Implements hook_views_default_views().
 */
function donate_entity_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'donate_entity';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'donate_entity';
  $view->human_name = 'Donate Entity';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Donate Entity';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'payment.payment.view.any';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Donate Entity: Donation Owner */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'donate_entity';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Donate Entity: Payment */
  $handler->display->display_options['relationships']['pid']['id'] = 'pid';
  $handler->display->display_options['relationships']['pid']['table'] = 'donate_entity';
  $handler->display->display_options['relationships']['pid']['field'] = 'pid';
  /* Field: Donate Entity: Donate entity ID */
  $handler->display->display_options['fields']['deid']['id'] = 'deid';
  $handler->display->display_options['fields']['deid']['table'] = 'donate_entity';
  $handler->display->display_options['fields']['deid']['field'] = 'deid';
  $handler->display->display_options['fields']['deid']['exclude'] = TRUE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'User';
  /* Field: Donate Entity: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'donate_entity';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Donate Entity: Created Date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'donate_entity';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Donation Date';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Donate Entity: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'donate_entity';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  /* Field: Donate Entity: Edit Link */
  $handler->display->display_options['fields']['edit_donate_entity']['id'] = 'edit_donate_entity';
  $handler->display->display_options['fields']['edit_donate_entity']['table'] = 'donate_entity';
  $handler->display->display_options['fields']['edit_donate_entity']['field'] = 'edit_donate_entity';
  $handler->display->display_options['fields']['edit_donate_entity']['label'] = 'Edit Donation';
  /* Field: Payment: Operations */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'payment';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['relationship'] = 'pid';
  $handler->display->display_options['fields']['operations']['label'] = 'Payment';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'donation_page');
  $handler->display->display_options['path'] = 'admin/content/donate_entities';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Donations';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'donation_block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';

  $views[] = $view;
  return $views;

}
