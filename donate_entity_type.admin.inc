<?php

/**
 * @file
 * Donate entity type editing UI.
 */

/**
 * UI controller.
 */
class DonateEntityTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage donate entity types, including adding and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the donate_entity type editing form.
 */
function donate_entity_type_form($form, &$form_state, $donate_entity_type, $op = 'edit') {
  if ($op == 'clone') {
    $donate_entity_type->label .= ' (cloned)';
    $donate_entity_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $donate_entity_type->label,
    '#description' => t('The human-readable name of this donate entity type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['intro'] = array(
    '#title' => t('Introduction'),
    '#type' => 'text_format',
    '#default_value' => $donate_entity_type->intro,
    '#description' => t('The text to appear above the donation fields.'),
    '#required' => FALSE,
    '#format' => empty($donate_entity_type->intro_format) ? 'filtered_html' : $donate_entity_type->intro_format,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $donate_entity_type->description,
    '#description' => t('The description of this donate entity type.'),
    '#required' => FALSE,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($donate_entity_type->type) ? $donate_entity_type->type : '',
    '#maxlength' => 32,
    // '#disabled' => $donate_entity_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'donate_entity_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this donate entity type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['suggestamt'] = array(
    '#type' => 'textarea',
    '#title' => t('Suggested Amount'),
    '#description' => t('Enter a new amount on each line. Each amount entered will appear as a list of default donation amounts to select from.'),
    '#default_value' => isset($donate_entity_type->suggestamt) ? $donate_entity_type->suggestamt : '',
  );
  $form['otheramt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check if the user should be able to enter there own donation amount'),
    '#default_value' => $donate_entity_type->otheramt,
  );
  $form['currency_code'] = array(
    '#type' => 'select',
    '#title' => t('The default currency code'),
    '#default_value' => $donate_entity_type->currency_code,
    '#options' => _donate_entity_get_currency_codes(),
  );
  $form['pmid'] = array(
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#default_value' => $donate_entity_type->pmid,
    '#options' => payment_method_options(),
    '#required' => TRUE,
  );
  $form['thankyou'] = array(
    '#title' => t('Thank You'),
    '#type' => 'text_format',
    '#default_value' => $donate_entity_type->thankyou,
    '#description' => t('Optional text that appears after the user has submitted the form. Use the "Thank You - Type" field to determine how this is displayed to the user.'),
    '#format' => empty($donate_entity_type->thankyou_format) ? 'filtered_html' : $donate_entity_type->thankyou_format,
  );
  $options = array(
    'dsm' => t('DSM'),
    'normal' => t('Normal'),
  );
  $form['thankyou_type'] = array(
    '#title' => t('Thank You - Type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $donate_entity_type->thankyou_type,
    '#description' => t('DSM: The message will always display in a drupal_set_message().') . "<br />" . t('Normal: The message will display above the form. This is ignored if the Redirect field is filled in; in which case the message will not appear.'),
  );
  $form['redirect'] = array(
    '#title' => t('Redirect'),
    '#type' => 'textfield',
    '#default_value' => $donate_entity_type->redirect,
    '#description' => t('Where the form redirects to on success, you can use this for your thank you page. If this is not set, the site will stay on the donations page after success.'),
    '#required' => FALSE,
  );
  $form['email_config'] = array(
    '#title' => t('Email Config'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'email_display' => array(
      '#type' => 'checkbox',
      '#title' => t('Show the email field on the form'),
      '#default_value' => $donate_entity_type->data['email']['email_display'],
    ),
    'email_required' => array(
      '#type' => 'checkbox',
      '#title' => t('Make the email field required'),
      '#default_value' => $donate_entity_type->data['email']['email_required'],
      '#states'        => array(
        'visible' => array(
          ':input[name="email_display"]' => array('checked' => TRUE),
        ),
      ),
    ),
    'email_match' => array(
      '#type' => 'checkbox',
      '#title' => t('Match the email address to a user account (if available) when a donation is saved'),
      '#default_value' => $donate_entity_type->data['email']['email_match'],
    ),
  );

  $form['data']['#tree'] = TRUE;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Donate Entity Type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function donate_entity_type_form_validate(&$form, &$form_state) {
  $suggestamt = explode("\n", $form_state['values']['suggestamt']);
  $retsuggestamt = array();
  foreach ($suggestamt as $amt) {
    if (trim($amt) != '') {
      if (!_donate_entity_is_cur_numeric($amt)) {
        form_set_error('suggestamt', t('The Suggested Amount must have a single numeric value per line. The following currency symbols are accepted ($,€,£,¥).'));
      }
      $retsuggestamt[] = trim($amt);
    }
  }
  $form_state['values']['suggestamt'] = implode("\n", $retsuggestamt);
}

/**
 * Form API submit callback for the type form.
 */
function donate_entity_type_form_submit(&$form, &$form_state) {
  $form_state['values']['intro_format'] = $form_state['values']['intro']['format'];
  $form_state['values']['intro'] = $form_state['values']['intro']['value'];
  $form_state['values']['thankyou_format'] = $form_state['values']['thankyou']['format'];
  $form_state['values']['thankyou'] = $form_state['values']['thankyou']['value'];
  $donate_entity_type = entity_ui_form_submit_build_entity($form, $form_state);
  
  // Save the email configuration.
  $email_config = array();
  foreach ($form_state['values'] as $key => $value) {
    if (substr($key, 0, 6) == 'email_') {
      $email_config[$key] = $value;
    }
  }
  $donate_entity_type->data['email'] = $email_config;
  
  $donate_entity_type->save();
  $form_state['redirect'] = 'admin/structure/donate_entity_type';
}

/**
 * Form API submit callback for the delete button.
 */
function donate_entity_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/donate_entity_type/manage/' . $form_state['donate_entity_type']->type . '/delete';
}
